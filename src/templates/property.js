import React from "react"
import { graphql } from "gatsby"
import styled from "@emotion/styled"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Content = styled.div`
  margin: 0 auto;
  max-width: 860px;
  padding: 1.45rem 1.0875rem;
`

export default ({ data }) => {
    console.log(`data...${data}`);
  const property = data.wordpressWpProperties
  return (
    <Layout>
      <SEO
        title={property.title}
        description={"Desc"}
      />
      <Content>
       <h1>{property.title}</h1>
       <h2 
       dangerouslySetInnerHTML={{ __html: property.price_formatted }}></h2>
       <img src={property.featured_media.source_url} />
      </Content>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($id: String!) {
    wordpressWpProperties(id: { eq: $id }) {
      title
      price_formatted
      featured_media {
        source_url
      }
    }
  }
`


// export const pageQuery = graphql`
//   query($path: String!) {
//     markdownRemark(frontmatter: { path: { eq: $path } }) {
//       html
//       excerpt(pruneLength: 160)
//       frontmatter {
//         date(formatString: "DD MMMM, YYYY")
//         path
//         title
//       }
//       fields {
//         readingTime {
//           text
//         }
//       }
//     }
//   }
// `
